#! /bin/bash

endpoint=$1
propresource=$2
propname=$3
sensor=$4
propid=$5
dbname=$6
mongoeval=$7
url=$8

echo "Starting query script"
echo "Requesting mongo database $dbname at $url!"

echo "Query to execute: $mongoeval"

result="$(mongo $url/$dbname --eval $mongoeval --quiet)"
echo $result

json="{
  \"kind\" : \"http://schemas.ugoe.cs.rwm/monitoring#monitorableproperty\",
  \"attributes\" : {
    \"monitoring.property\" : \"$propname\",
    \"monitoring.result\" : \"$result\"
  },
  \"source\" : {
    \"location\" : \"$sensor\"
  },
  \"target\" : {
    \"location\" : \"$propresource\"
  }
}"


echo $json

echo "Publishing new results"
#curl -v -X PUT http://"$endpoint""$propid" -H 'Content-Type: text/occi'  -H 'Category: monitorableproperty; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="'$sensor'",occi.core.target="'$propresource'", occi.core.title="monProp", monitoring.result="'$result'", monitoring.property="'$propname'"' 

#echo curl -v -X PUT http://"$endpoint""$propid" -H 'Content-Type: text/occi'  -H 'Category: monitorableproperty; scheme="http://schemas.ugoe.cs.rwm/monitoring#"; class="kind"'  -H 'X-OCCI-Attribute:occi.core.source="'$sensor'",occi.core.target="'$propresource'", occi.core.title="monProp", monitoring.result="'$result'", monitoring.property="'$propname'"' 


curl -v -X PUT -d "$json" http://"$endpoint""$propid" -H 'Content-Type: application/json'

