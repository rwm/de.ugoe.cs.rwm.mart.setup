#!/bin/bash
# This script configures kibana to connect to the elasticsearch server
# to access data and to export the app url on port 5601:
# The environment variable elasticsearch_ip and kibana_ip are expected
# to be set up.
sed -i 's/^#server.host.*/server.host: '$kibana_ip'/' /etc/kibana/kibana.yml
sed -i 's/^#elasticsearch.hosts.*/elasticsearch.hosts: ["http:\/\/'$elasticsearch_ip':9200"]/' /etc/kibana/kibana.yml
