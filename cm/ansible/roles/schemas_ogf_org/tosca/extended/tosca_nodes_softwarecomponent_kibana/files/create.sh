#!/bin/bash
# This script installs java and elasticsearch

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

apt-get update
apt-get install -y kibana

# set up to run as service
update-rc.d kibana defaults 95 10

