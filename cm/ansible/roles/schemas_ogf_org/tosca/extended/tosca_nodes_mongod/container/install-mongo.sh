#!/bin/bash

echo "BEGIN mongo install"

if [ -e /proc/1/cgroup ] ; then
  if grep docker /proc/1/cgroup -qa; then
     # docker messes up the init system so we have to perform specific init
     dpkg-divert --local --rename --add /sbin/initctl
     ln -s /bin/true /sbin/initctl
     dpkg-divert --local --rename --add /etc/init.d/mongod
     ln -s /bin/true /etc/init.d/mongod
  fi
fi

echo "add mongo key to source list"

# sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4

#echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.2.list

echo "apt-get update"
sudo apt-get update -y

echo "apt-get install -y mongodb-org"
sudo apt-get install -yq mongodb

echo "END mongo install"
