#!/bin/bash

echo "BEGIN configure mongo"

sudo service mongodb stop

echo "change mongo bind ip to ${DB_IP}"
sudo sed -i "s/bind_ip.*/bind_ip = ${DB_IP}/" /etc/mongodb.conf

if [ ! -z "$DB_PORT" ] ; then
  echo "change mongo bind port to ${DB_PORT}"
  sudo sed -i "s/port.*/port = ${DB_PORT}/" /etc/mongodb.conf
fi

echo "END configure mongo"

