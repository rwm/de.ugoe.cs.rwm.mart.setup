#! /bin/bash

avgCpu=0
slept=0
sequence=3
times=3
send=$(python -c "print $sequence*$times")
url=http://$1:61208/api/2/cpu/total

if [ -f ~/monitor.txt ]; then
    echo "Removing previous monitoring data"
    rm monitor.txt
fi

echo "Starting monitoring script"
echo "Requesting $url every $sequence seconds aggregating after $times times!"


while true; do
    cpu=$(curl -s $url | awk '{print $2}')
    cutcpu=$(echo "$cpu" | rev | cut -c 2- | rev)
    sumCpu=$(python -c "print $sumCpu+$cutcpu")
    echo "Current Cpu: $cutcpu"
    sleep $sequence
    slept=$(($slept+$sequence))
    if [ "$slept" = $send ]; then
       mid=$(python -c "print $sumCpu/$times")
       echo "Aggregate Cpu: $mid"
       echo $mid >> ~/monitor.txt
       slept=0
       sumCpu=0
    fi
done
