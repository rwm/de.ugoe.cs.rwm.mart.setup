#!/bin/bash
# This script installs java, logstash and the contrib package for logstash
# install java as prereq

apt-get update
apt-get install -y openjdk-8-jre-headless
mkdir /etc/logstash

# install by apt-get from repo
wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

apt-get update
apt-get install -y logstash

# install contrib to get the relp plugin
/usr/share/logstash/bin/logstash-plugin install logstash-input-relp
