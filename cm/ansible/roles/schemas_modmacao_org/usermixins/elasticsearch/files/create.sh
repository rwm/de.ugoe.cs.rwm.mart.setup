#!/bin/bash
# This script installs java and elasticsearch

apt-get update
apt-get install -y openjdk-8-jre-headless

wget -qO - https://artifacts.elastic.co/GPG-KEY-elasticsearch | sudo apt-key add -
echo "deb https://artifacts.elastic.co/packages/7.x/apt stable main" | sudo tee -a /etc/apt/sources.list.d/elastic-7.x.list

apt-get update
apt-get install -y elasticsearch

# set up to run as service
update-rc.d elasticsearch defaults 95 10

