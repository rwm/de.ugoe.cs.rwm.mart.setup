Upload install apache:
  file.managed:
     - name: /home/ubuntu/install_apache.sh
     - source: salt://tosca_nodes_apache/files/install_apache.sh
     - mode: 0777

Upload start apache:
  file.managed:
     - name: /home/ubuntu/start_apache.sh
     - source: salt://tosca_nodes_apache/files/start_apache.sh
     - mode: 0777

