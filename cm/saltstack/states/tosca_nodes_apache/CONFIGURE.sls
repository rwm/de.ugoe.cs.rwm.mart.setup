Set PORT env:
  cmd.run:
     - name: export PORT={{ pillar['port'] }}

Set DOC_ROOT env:
  environ.setenv:
     - name: DOC_ROOT
     - value: {{ pillar['document_root'] }}

Run install apache:
  cmd.run:
     - name: /home/ubuntu/install_apache.sh
     - cwd: /
     - stateful: True
